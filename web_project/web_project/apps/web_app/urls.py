from django.urls import path
from . import views


urlpatterns = [
    path('', views.main, name='main'),
    path('visited_links', views.save, name='save'),
    path('visited_domains', views.show, name='show'),
]
