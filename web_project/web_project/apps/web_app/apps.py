from django.apps import AppConfig


class WebAppConfig(AppConfig):
    name = 'web_app'
    verbose_name = 'Учёт посещений'
