from django.db import models


class Link(models.Model):
    link = models.TextField('Посещенная ссылка')
    domain = models.TextField('Посещенный домен')
    visit_date = models.DateTimeField('Время посещения')

