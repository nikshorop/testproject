from django.test import TestCase, Client
from web_test_project.apps.web_app.models import Link
from django.http import JsonResponse


class web_app_Test(TestCase):
    @classmethod
    def setUpTestData(cls):
        Link.objects.create(link='https://metanit.com/python/django/1.1.php\nhttps://habr.com/ru/post/169381/',
                            domain='', visit_date='')
        Link.objects.create(link='здесьдолжнабытьссылка\nhttps://habr.com/ru/post/169381/',
                            domain='', visit_date='')
        print("Test data initialized!")

    def test_save_links_status_ok(self):
        # arrange
        client = Client()
        obj = Link.objects.get(id=1)
        true_resp = JsonResponse({"status": "ok"})
        # act
        resp = client.post('/visited_links/', {"links": obj.link})
        # assert
        self.assertEqual(true_resp, resp)

    def test_save_no_links_status_not_ok(self):
        # arrange
        client = Client()
        obj = Link.objects.get(id=2)
        error = obj.link.split('\n')[0]
        true_resp = JsonResponse({"status": "not ok", "not valid links": error})
        # act
        resp = client.post('/visited_links/', {"links": obj.link})
        # assert
        self.assertEqual(true_resp, resp)