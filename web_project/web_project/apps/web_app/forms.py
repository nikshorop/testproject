from django import forms
from django.utils import timezone
from django.utils.timezone import activate
from django.http import JsonResponse
from django.conf import settings
activate(settings.TIME_ZONE)


class PostForm(forms.Form):
    links = forms.CharField(widget=forms.Textarea, label="Посещенные ссылки")


class GetForm(forms.Form):
    from_ = forms.DateTimeField(label="С", initial=timezone.now().strftime("%d-%m-%Y %H:%M:%S"))
    to_ = forms.DateTimeField(label="До", initial=timezone.now().strftime("%d-%m-%Y %H:%M:%S"))


