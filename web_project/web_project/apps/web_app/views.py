from django.shortcuts import render
from .forms import PostForm, GetForm
from django.http import JsonResponse, HttpResponse
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from urllib.parse import urlparse
from django.core.cache import cache
from django.conf import settings
from django.utils import timezone
from redis import StrictRedis
from datetime import datetime

# создание объекта для работы с бд
cache = StrictRedis(host='localhost', port=6379, db=1)


# создание форм на главной странице
def main(request):
    # форма для отправки ссылок
    post_form = PostForm()
    # форма для указания периода времени посещений
    get_form = GetForm()
    data = {"form_post": post_form, "form_get": get_form}
    return render(request, 'web_app/main.html', context=data)


# метод обработки GET запроса для получения уникальных доменов
# посещенных за указанный период
def show(request):
    if request.method == "GET":
        # запись параметров запроса
        time_from = request.GET.get('from_')
        time_to = request.GET.get('to_')
        # запись доменов и статуса обработки
        domains, status = redis_read(time_from, time_to)
        return JsonResponse({"domains": domains, "status": status})


def save(request):
    if request.method == "POST":
        # фиксация времени получения запроса
        timezone.activate(settings.TIME_ZONE)
        time = timezone.now().strftime("%d-%m-%Y %H:%M:%S")
        # парсинг отправленных ссылок
        json_string = request.POST.get('links')
        links = json_string.replace('\r', '').replace(' ', '').split('\n')

        # проверка ссылок на валидность
        validate = URLValidator()
        errors_in_links = []
        for link in links:
            try:
                validate(link)
            except ValidationError:
                errors_in_links.append(link)
        # сообщаем пользователю о невалидных ссылках
        if len(errors_in_links) > 0:
            return JsonResponse({"status": "not ok", "not valid links": errors_in_links})
        # если все ссылки валидны, записыпаем домены в бд
        else:
            domains = []
            try:
                # получение доменов из ссылок
                for link in links:
                    domains.append(urlparse(link).netloc)
                # запись доменов и времени в бд redis
                redis_write(domains, time)
                # сообщение системы об успешной обработке
                return JsonResponse({"status": "ok"})
            except:
                # сообщение системы об ошибке во время записи данных
                return JsonResponse({"status": "error at Redis"})


# поиск доменов, посещённых за указанный период
def redis_read(dt_from, dt_to):
    # преобразование указанного времени в datatime тип для сравения
    dt_from = datetime.strptime(dt_from, "%d-%m-%Y %H:%M:%S")
    dt_to = datetime.strptime(dt_to, "%d-%m-%Y %H:%M:%S")
    # список подходящих уникальных доменов
    domains = []
    # в Redis ключом является домен,
    # значение - список дат и времени посещений
    # для каждого ключа(домена) просматриваем все моменты посещений
    for domain in cache.scan_iter(match='*'):
        for i in range(0, cache.llen(domain)):
            dt = cache.lindex(domain, i).decode("utf-8")
            dt = datetime.strptime(dt, "%d-%m-%Y %H:%M:%S")
            # если посещение было в указанный период
            # записываем домен и останавливаем проход по его значениям
            if dt_from <= dt <= dt_to:
                domains.append(domain.decode("utf-8"))
                break
    # если домены найдены, возвращаем и сообщаем о успешной обработке
    if len(domains) > 0:
        return domains, "ok"
    # если не найдены, возвращаем сообщение об этой ошибке
    else:
        return "none", "no visits for that period"

# запись посещенных доменов в Redis и время их посещения
def redis_write(domains, dt):
    for domain in domains:
        cache.lpush(domain, dt)




